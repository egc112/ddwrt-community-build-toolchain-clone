The download and installation instructions are in the [Wiki](https://gitlab.com/egc112/ddwrt-community-build-clone/-/wikis/How-to-setup-a-Build-Machine/1.-Installing-Ubuntu-and-Build-Environment)

Our [main website](https://gitlab.com/egc112/ddwrt-community-build-clone/-/tree/master) houses the patches/scripts/files/wikis etc.

